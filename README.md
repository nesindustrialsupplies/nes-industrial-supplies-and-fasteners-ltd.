We supply a range of fixings, fasteners, PPE and industrial supplies. Metric and imperial fasteners including UNC, UNF, BSF, BSW and BA. These include hex head bolts, screws, nuts, washers and many more fixings and fasteners. We can also offer reverse engineering and parts manufactured to drawing.

Address: Hazeldine House, Plantation Road, Newstead Industrial Estate, Trentham, Stoke-on-Trent ST4 8HX, United Kingdom

Phone: +44 1782 646932

Website: https://nenutsboltsfasteners.co.uk
